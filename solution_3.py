def find_gcd(a, b):
    if a == 0:
        return b
    if b == 0:
        return a
    while b != 0:
        r = a % b
        a = b
        b = r
    return a


n1 = int(input("First Number? "))
n2 = int(input("Second Number? "))

result = find_gcd(n1, n2)
print("GCD(", n1, ",", n2, ") =", result)
