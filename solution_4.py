def get_row(row_height):
    row = ""  # initially the 'row' string is empty
    for i in range(1, row_height + 1):  # need to fill the string starting from 1, and go up to 'height'
        row += str(i) + " "
    for i in range(row_height - 1, 0, -1):  # once reached 'height', need to go down to 1
        row += str(i) + " "
    return row


def print_triangle(height):
    for currentRow in range(1, height + 1):
        # each row has space at the beginning (2 spaces * line number)
        for space in range(0, height - currentRow):
            print("  ", end="")  # extra two spaces for each row
        rowStr = get_row(currentRow)
        print(rowStr)


####

for height in [4, 2, 7]:
    print("Height = ", height)
    print_triangle(height)
    print()
   