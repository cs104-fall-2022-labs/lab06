import random


def dice_sum():
    inp = int(input("Please enter the desired sum: "))
    while inp < 2 or inp > 12:
        print("Desired dice sum:", inp)
        print("The sum can be between 2 and 12, so please try again.")
        inp = int(input("Please enter the desired sum: "))

    print("Desired dice sum:", inp)

    dice1 = random.randint(1, 6)
    dice2 = random.randint(1, 6)
    while dice1 + dice2 != inp:
        print(dice1, "and", dice2, "=", dice1 + dice2)
        dice1 = random.randint(1, 6)
        dice2 = random.randint(1, 6)
    print(dice1, "and", dice2, "=", dice1 + dice2)
    print("Got it! Stopping..")


dice_sum()
